import React, { Component } from 'react';
import './bootstrap-3.3.7-dist/css/bootstrap.css';
import './custom.css'
import Request from 'superagent';
import Panel from 'react-bootstrap/lib/Panel';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';

class App extends Component {


    constructor(){
        super();
        this.state = {};
    }

    componentWillMount(){



        Request.get('data/').set('Content-Type', 'application/json').then((response) => {
            var data = JSON.parse(response.text);
            this.setState({
                boilerrooms : JSON.parse(data)
            })
        })




    }






    render() {
        const sessions = this.state.boilerrooms;


    return (


      <div className="BoilerRoomApp">
        <div className="App-header">
          <h2>My Favorite Boiler Room Sessions</h2>
            <h2>{console.log(sessions)}</h2>



        </div>

        <div className="BoilerRoomList">
            <PanelGroup accordion>
                <Panel header="VRIL" eventKey="1">
                    <p>VRIL Boiler Room Berlin Live Set</p>
                    <p>Berlin</p>
                    <p>Live Set</p>
                </Panel>
                <Panel header="Ikonika" eventKey="2">
                    <p>Ikonika Tribute to DJ Rashad Boiler Room London DJ Set</p>
                    <p>London</p>
                    <p>DJ Set</p>
                </Panel>
                <Panel header="PERM" eventKey="3">
                    <p>PERM Boiler Room Leipzig Live Set</p>
                    <p>Leipzig</p>
                    <p>Live Set</p>
                </Panel>
            </PanelGroup>
        </div>


      </div>
    );
  }



}



export default App;
