const express = require('express');
const fs = require('fs');

const app = express();

app.set('port', (process.env.PORT || 3001));




app.get('/data', function (req, res) {
    fs.readFile("data.json", 'utf8', function (err, data) {
        console.log( data );
        res.json( data );
    });
})

app.listen(3001, function () {
    console.log('Example app listening on port 3001!')
})