Programming problem:

Build a simple node server + React web/native app that displays information about your three favourite Boiler Room sets. You don't need to use a database (just use plain javascript objects in your server side code), but you should make a network request to your node server to fetch information about the sets (including but not limited to title, artist, and location), and then display them in a simple list. You can use any server/client setup you choose, but please keep it simple.

Feel free to be creative and take your time, bear in mind this test is normally for our full stack engineers. Email us with any questions you have!